To download these instructions run the following from a cluster node:
curl -O https://bitbucket.org/caltechimss/central-hpc-public/raw/8ea2022ddd1dac46d9ecbe0a2dbb93732a076eb7/docs/setup-miniconda.md

Prepare installation location in your group directory:

mkdir -p /central/groups/your-group-dir/your-cluster-username/miniconda3

Download MiniConda:

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

sh ./Miniconda3-latest-Linux-x86_64.sh

<enter to continue>
<yes to agreement>

Specify /central/groups/your-group-dir/your-cluster-username/miniconda3 as install location.

<yes to initialize by default>

Log out and back in to enable the conda tools. 

Note: If selecting 'yes' to initialize conda upon login, it will activate your 'base' environment and whatever packages installed within in. If you've created new environments, you'll need to activate those within your slurm submission scripts otherwise you'll end up with base activated. 


At this point you may create your first environment and install packages into it.

conda create --name my-first-conda-env

conda activate my-first-conda-env

Now that your first environment is active, you may install packages to it.

conda install XXX (whatever conda packages needed, avoid using pip unless needed for some reason, ie broken conda package or something not available via conda package management system)

Now that your new have your environment setup, add the activate command to whatever slurm script you have *before* your final command to run a program. i.e.

In runjob.sh

conda activate my-first-conda-env
which python # verifies that you are in fact pointing to your activated conda environment
python ./my-python-script

If not already, I recommend familiarizing yourself with both conda and python venv systems.

https://conda.io/projects/conda/en/latest/user-guide/getting-started.html
https://docs.python.org/3/tutorial/venv.html

Here's a slurm submission script template you can edit as needed.
https://bitbucket.org/caltechimss/central-hpc-public/raw/master/slurm-scripts/extended-slurm-submission-script

You can pull it down to the cluster with:
wget https://bitbucket.org/caltechimss/central-hpc-public/raw/master/slurm-scripts/extended-slurm-submission-script

John



