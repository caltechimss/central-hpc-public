#### Install miniconda.

It's recommended to use Python environments (Conda, Python venv's etc) in conjunction with the OOD Jupyter app, rather than --user based package installations with pip).

Below is an example of of getting setup with Miniconda, installing some packages and then registering ipykernel so it shows up as an option in our OOD Jupyter app. We advise installing in the group directories as the available storage quota is much higher than home. Standard Python package installation will not work on the cluster as users are not allowed to install packages into system directories. 

mkdir /central/groups/owwen/$USER (this will create a directory for your data in the groups dir to keep things clean)

wget [https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh](https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh) (download the miniconda installer)

sh ./Miniconda3-latest-Linux-x86_64.sh
(agree to license)

Specify /central/groups/owwen/$USER/miniconda3 for the installation directory.

(Log out and cluster and back in to activate base conda, assuming it was set to enable automatically)

Once you have the base Conda installation working, you may create a new Conda environment for use with the OOD Jupyter App, including all custom packages you require (package installion is optional at this point, you may safely remove packages if you intend to install later. Example below.

conda create -y -n my-new-jupyter-environment package1 package2 package3 etc.

conda activate my-new-jupyter-environment

conda install package1 package2 package3 (alternative to installing package during environment creation above)

python -m ipykernel install --user --name jupyter-env --display-name "Python (jupyter-env)"

Now that the ipykernel is installed it should be visible within the OOD Jupyter App. Relaunch the OOD Jupyter App, click New and select the newly registered kernel. Any additional packages may be installed by activating the new environment via terminal and installing with native conda commands or pip. New python package installation needs to occur *outside* of the Jupyter app itself, in a standard terminal session with the desired conda/python environment activated. This is due to the fact that the Jupyter interactive app will always point to its own custom python environment regardless of any ipykernel environments that happen to be available. 

Highly recommended links for those unfamiliar with Python virtual environments:

https://conda.io/projects/conda/en/latest/user-guide/getting-started.html

https://realpython.com/python-virtual-environments-a-primer/

John
