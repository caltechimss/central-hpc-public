Globus on the Resnick HPC

We employ a Globus endpoint enabled on the Resnick HPC which allows researchers to 
efficently move data from both client machines and other institution endpoints to the cluster
filesystem. 

For moving data between client machines and the Resnick cluster endpoint, we recommend
installing Globus Connect Personal. https://www.globus.org/globus-connect-personal

Once installed, you may search for "Resnick" in the File Transfer dialog box. This will 
bring up our endpoint "Caltech Resnick HPC Directories", which provides access
to both /home and /group directories. We strongly recommend transfering files only to
/group directories as the storage capacity is much greater than /home. 

Name: Resnick-HPC-Cluster
Direct Link: https://app.globus.org/file-manager/collections/5be0c110-3df6-4c24-ae62-7fd88da26e3b/overview
ID 5be0c110-3df6-4c24-ae62-7fd88da26e3b
UUID 9fc54b35-f66e-4ef0-a36a-49b20d684b99

Reference:

https://www.globus.org/what-we-do
https://www.globus.org/globus-connect-personal


